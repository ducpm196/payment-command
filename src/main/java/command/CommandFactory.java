package main.java.command;

import main.java.command.impl.*;
import main.java.constant.Commands;

public class CommandFactory {
    public Command getCommand(String commandName) {
        Commands enumCommand = Commands.valueOf(commandName);

        switch (enumCommand) {
            case CASH_IN:
                return new CashInCommand();
            case VIEW_BALANCE:
                return new ViewBalanceCommand();
            case CREATE_BILL:
                return new CreateBillCommand();
            case UPDATE_BILL:
                return new UpdateBillCommand();
            case DELETE_BILL:
                return new DeleteBillCommand();
            case LIST_BILL:
                return new ListBillCommand();
            case PAY:
                return new PayCommand();
            case DUE_DATE:
                return new DueDateCommand();
            case SCHEDULE:
                return new ScheduleCommand();
            case LIST_PAYMENT:
                return new ListPaymentCommand();
            case SEARCH_BILL_BY_PROVIDER:
                return new SearchBillByProviderCommand();
            case EXIT:
                return new ExitCommand();
            default:
                return null;
        }
    }
}
