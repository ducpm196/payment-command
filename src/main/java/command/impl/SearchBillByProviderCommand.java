package main.java.command.impl;

import main.java.constant.Constants;
import main.java.command.Command;
import main.java.model.Bill;
import main.java.model.Customer;

import java.text.SimpleDateFormat;
import java.util.List;

public class SearchBillByProviderCommand implements Command {
    @Override
    public void execute(String[] args) {
        if (args.length != 2) {
            System.out.println("Incorrect number of arguments. Usage: provider");
            return;
        }

        String provider = args[1];
        List<Bill> bills = Customer.getInstance().searchBillByProvider(provider);
        SimpleDateFormat dateFormat = new SimpleDateFormat(Constants.dateFormat);
        System.out.println(Constants.billHeader);
        for (Bill bill : bills) {
            String dueDateFormatted = dateFormat.format(bill.getDueDate());
            System.out.println(bill.getId() + ". " + bill.getType() + " " + bill.getAmount() + " " + dueDateFormatted + " " + bill.getStatus() + " " + bill.getProvider());
        }
    }
}
