package main.java.command.impl;

import main.java.constant.Constants;
import main.java.model.Bill;
import main.java.model.Customer;
import main.java.command.Command;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.List;

public class ListBillCommand implements Command {
    @Override
    public void execute(String[] args) {
        if (args.length != 1) {
            System.out.println("Incorrect number of arguments. Usage: only command");
            return;
        }

        List<Bill> bills = Customer.getInstance().listBills();

        SimpleDateFormat dateFormat = new SimpleDateFormat(Constants.dateFormat);
        DecimalFormat balanceFormat = new DecimalFormat("#");
        System.out.println(Constants.billHeader);

        for (Bill bill : bills) {
            String dueDateFormatted = dateFormat.format(bill.getDueDate());
            String formattedAmount = balanceFormat.format(bill.getAmount());
            System.out.println(bill.getId() + ". " + bill.getType() + " " + formattedAmount + " " + dueDateFormatted + " " + bill.getStatus() + " " + bill.getProvider());
        }
    }
}
