package main.java.command.impl;

import main.java.command.Command;
import main.java.constant.Constants;
import main.java.model.Customer;
import main.java.model.Payment;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.List;

public class ListPaymentCommand implements Command {
    @Override
    public void execute(String[] args) {
        if (args.length != 1) {
            System.out.println("Incorrect number of arguments. Usage: only command");
            return;
        }

        List<Payment> payments = Customer.getInstance().listPayments();

        SimpleDateFormat dateFormat = new SimpleDateFormat(Constants.dateFormat);
        DecimalFormat balanceFormat = new DecimalFormat("#");
        System.out.println(Constants.paymentHeader);

        for (Payment payment : payments) {
            String paymentDateFormatted = dateFormat.format(payment.getPaymentDate());
            String formattedAmount = balanceFormat.format(payment.getAmount());
            System.out.println(payment.getId() + ". " + formattedAmount + " " + paymentDateFormatted + " " + payment.getStatus() + " " + payment.getBillId());
        }
    }
}
