package main.java.command.impl;

import main.java.constant.Constants;
import main.java.constant.States;
import main.java.command.Command;
import main.java.model.Bill;
import main.java.model.Customer;

import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.List;

public class DueDateCommand implements Command {
    @Override
    public void execute(String[] args) {
        if (args.length != 1) {
            System.out.println("Incorrect number of arguments. Usage: only command");
            return;
        }

        List<Bill> bills = Customer.getInstance().listBills();

        SimpleDateFormat dateFormat = new SimpleDateFormat(Constants.dateFormat);
        System.out.println(Constants.billHeader);

        bills.stream()
                .filter(bill -> !bill.getStatus().equalsIgnoreCase(States.PROCESSED.name()))
                .sorted(Comparator.comparing(Bill::getDueDate))
                .forEach(bill -> {
                    String dueDateFormatted = dateFormat.format(bill.getDueDate());
                    System.out.println(bill.getId() + ". " + bill.getType() + " " + bill.getAmount() + " " + dueDateFormatted + " " + bill.getStatus() + " " + bill.getProvider());
                });
    }
}
