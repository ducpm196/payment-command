package main.java.command.impl;

import main.java.command.Command;
import main.java.model.Customer;

import java.time.format.DateTimeParseException;

public class ScheduleCommand implements Command {
    @Override
    public void execute(String[] args) {
        if (args.length != 3) {
            System.out.println("Incorrect number of arguments. Usage: billId scheduleDate");
            return;
        }

        try {
            int billId = Integer.parseInt(args[1]);
            String scheduleDateStr = args[2];
            Customer.getInstance().schedulePayment(billId, scheduleDateStr);
        } catch (NumberFormatException e) {
            System.out.println("Invalid billId format. Please provide a valid numeric value.");
        } catch (DateTimeParseException e) {
            System.out.println("Invalid date format. Please use the format dd/MM/yyyy");
        }
    }
}
