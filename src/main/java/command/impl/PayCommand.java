package main.java.command.impl;

import main.java.command.Command;
import main.java.model.Customer;

import java.util.Arrays;

public class PayCommand implements Command {
    @Override
    public void execute(String[] args) {
        try {
            Integer[] billIds = Arrays.stream(args)
                    .skip(1)
                    .map(Integer::parseInt)
                    .toArray(Integer[]::new);
            Customer.getInstance().payBill(billIds);
        } catch (NumberFormatException e) {
            System.out.println("Invalid billId format. Please provide valid numeric values.");
        }
    }
}
