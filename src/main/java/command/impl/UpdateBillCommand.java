package main.java.command.impl;

import main.java.command.Command;
import main.java.constant.Constants;
import main.java.model.Customer;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Date;

public class UpdateBillCommand implements Command {
    @Override
    public void execute(String[] args) {
        if (args.length != 6) {
            System.out.println("Incorrect number of arguments. Usage: billId type amount dueDate provider");
            return;
        }
        try {
            int billId = Integer.parseInt(args[1]);
            String type = args[2];
            BigDecimal amount = BigDecimal.valueOf(Double.parseDouble(args[3]));
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(Constants.dateFormat);
            LocalDate dueDate = LocalDate.parse(args[4], formatter);
            String provider = args[5];
            Customer.getInstance().updateBill(billId, type, amount, Date.from(dueDate.atStartOfDay(ZoneId.systemDefault()).toInstant()), provider);
        } catch (NumberFormatException e) {
            System.out.println("Invalid amount format. Please provide a valid numeric value.");
        } catch (DateTimeParseException e) {
            System.out.println("Invalid date format. Please use the format dd/MM/yyyy");
        }
    }
}