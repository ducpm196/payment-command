package main.java.command.impl;

import main.java.command.Command;
import main.java.model.Customer;

public class DeleteBillCommand implements Command {
    @Override
    public void execute(String[] args) {
        if (args.length != 2) {
            System.out.println("Incorrect number of arguments. Usage: billId");
            return;
        }

        try {
            int billId = Integer.parseInt(args[1]);
            Customer.getInstance().deleteBill(billId);
        } catch (NumberFormatException e) {
            System.out.println("Invalid billId format. Please provide a valid numeric value.");
        }
    }
}
