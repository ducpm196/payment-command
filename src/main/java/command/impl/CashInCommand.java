package main.java.command.impl;

import main.java.command.Command;
import main.java.model.Customer;

import java.math.BigDecimal;
import java.text.DecimalFormat;

public class CashInCommand implements Command {
    @Override
    public void execute(String[] args) {
        if (args.length < 2) {
            System.out.println("Invalid input. Please provide the amount to add.");
            return;
        }

        try {
            BigDecimal amount = BigDecimal.valueOf(Double.parseDouble(args[1]));
            Customer customer = Customer.getInstance();
            Customer.getInstance().addFunds(amount);

            DecimalFormat balanceFormat = new DecimalFormat("#");
            String formattedBalance = balanceFormat.format(customer.getBalance());
            System.out.println("Your available balance: " + formattedBalance);
        } catch (NumberFormatException e) {
            System.out.println("Invalid amount format. Please provide a valid number.");
        }
    }
}
