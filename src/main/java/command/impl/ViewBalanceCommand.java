package main.java.command.impl;

import main.java.command.Command;
import main.java.model.Customer;

import java.math.BigDecimal;
import java.text.DecimalFormat;

public class ViewBalanceCommand implements Command {
    @Override
    public void execute(String[] args) {
        if (args.length != 1) {
            System.out.println("Incorrect number of arguments. Usage: only command");
            return;
        }

        BigDecimal balance = Customer.getInstance().getBalance();
        DecimalFormat balanceFormat = new DecimalFormat("#");
        String formattedBalance = balanceFormat.format(balance);
        System.out.println("Your current balance: " + formattedBalance);
    }
}

