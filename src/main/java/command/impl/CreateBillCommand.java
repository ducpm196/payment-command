package main.java.command.impl;

import main.java.constant.Constants;
import main.java.command.Command;
import main.java.model.Customer;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Date;

public class CreateBillCommand implements Command {
    @Override
    public void execute(String[] args) {
        if (args.length < 5) {
            System.out.println("Insufficient arguments. Usage: type amount dueDate provider");
            return;
        }
        try {
            String type = args[1];
            BigDecimal amount = BigDecimal.valueOf(Double.parseDouble(args[2]));
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(Constants.dateFormat);
            LocalDate dueDate = LocalDate.parse(args[3], formatter);
            String provider = args[4];
            Customer.getInstance().createBill(type, amount, Date.from(dueDate.atStartOfDay(ZoneId.systemDefault()).toInstant()), provider);
        } catch (NumberFormatException e) {
            System.out.println("Invalid amount format. Please provide a valid numeric value.");
        } catch (DateTimeParseException e) {
            System.out.println("Invalid date format. Please use the format dd/MM/yyyy");
        }
    }
}
