package main.java;

import main.java.command.Command;
import main.java.command.CommandFactory;
import main.java.constant.Commands;

import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        CommandFactory factory = new CommandFactory();

        System.out.println("Available commands:");
        for (Commands command : Commands.values()) {
            System.out.print("- " + command.name() + " ");
        }
        System.out.println();

        while (true) {
            System.out.print("$ ");
            String userInput = scanner.nextLine();
            String[] userArgs = userInput.split(" ");

            if (userArgs.length == 0) {
                System.out.println("Please provide a command.");
                continue;
            }

            String commandName = userArgs[0];
            if (Arrays.stream(Commands.values()).map(Enum::name).anyMatch(commandName::equals)) {
                Command command = factory.getCommand(commandName);
                if (command != null) {
                    command.execute(userArgs);
                } else {
                    System.out.println("Invalid command: " + commandName);
                }
            } else {
                System.out.println("Invalid command: " + commandName);
            }

            if (commandName.equals(Commands.EXIT.name())) {
                System.out.println("Good bye!");
                break;
            }
        }

        scanner.close();
    }
}