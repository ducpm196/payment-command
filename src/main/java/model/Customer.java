package main.java.model;

import main.java.constant.Constants;
import main.java.constant.States;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

public class Customer {
    private static Customer instance;
    private BigDecimal balance;
    private List<Bill> bills;
    private List<Payment> payments;

    private Customer() {
        this.balance = BigDecimal.valueOf(0);
        this.bills = new ArrayList<>();
        this.payments = new ArrayList<>();

        SimpleDateFormat dateFormat = new SimpleDateFormat(Constants.dateFormat);
        try {
            this.bills.add(new Bill(1, "ELECTRIC", BigDecimal.valueOf(200000), dateFormat.parse("25/07/2023"), "EVN HCMC"));
            this.bills.add(new Bill(2, "WATER", BigDecimal.valueOf(175000), dateFormat.parse("30/07/2023"), "SAVACO HCMC"));
            this.bills.add(new Bill(3, "INTERNET", BigDecimal.valueOf(800000), dateFormat.parse("30/09/2023"), "VNPT"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    // Singleton pattern to ensure only one instance
    public static Customer getInstance() {
        if (instance == null) {
            instance = new Customer();
        }
        return instance;
    }

    public void addFunds(BigDecimal amount) {
        this.balance = this.balance.add(amount);
    }

    public void createBill(String type, BigDecimal amount, Date dueDate, String provider) {
        int newBillId = bills.size() + 1;
        Bill newBill = new Bill(newBillId, type, amount, dueDate, provider);
        bills.add(newBill);

        System.out.println("Bill created with ID: " + newBillId);
    }

    public void deleteBill(int billId) {
        Bill billToDelete = findBillById(billId);
        if (billToDelete != null) {
            bills.remove(billToDelete);
            System.out.println("Bill with ID " + billId + " deleted.");
        } else {
            System.out.println("Bill with ID " + billId + " not found.");
        }
    }

    public void updateBill(int billId, String type, BigDecimal amount, Date dueDate, String provider) {
        Bill billToUpdate = findBillById(billId);
        if (billToUpdate != null) {
            billToUpdate.setType(type);
            billToUpdate.setAmount(amount);
            billToUpdate.setDueDate(dueDate);
            billToUpdate.setProvider(provider);
            System.out.println("Bill with ID " + billId + " updated.");
        } else {
            System.out.println("Bill with ID " + billId + " not found.");
        }
    }

    public Bill findBillById(int id) {
        for (Bill bill : bills) {
            if (bill.getId() == id) {
                return bill;
            }
        }
        return null;
    }

    public Payment findPaymentById(int id) {
        for (Payment payment : payments) {
            if (payment.getId() == id) {
                return payment;
            }
        }
        return null;
    }

    public List<Bill> listBills() {
        return this.bills;
    }

    public void payBill(Integer[] billIds) {
        // Ưu tiên thanh toán hóa đơn có ngày đáo hạn gần nhất trước
        Arrays.sort(billIds, Comparator.comparingInt(billId -> {
            Bill bill = findBillById(billId);
            return (bill != null) ? bill.getDueDate().compareTo(new Date()) : 0;
        }));

        for (Integer billId : billIds) {
            Bill bill = findBillById(billId);
            if (bill == null) {
                System.out.println("Sorry! Not found a bill with id " + billId);
                continue;
            }
            if (bill.getStatus().equals(States.PROCESSED.name())) {
                System.out.println("Bill with id " + billId + " is already paid.");
                continue;
            }
            if (bill.getAmount().compareTo(balance) <= 0) {
                balance = balance.subtract(bill.getAmount());
                bill.setStatus(States.PROCESSED.name());
                payments.add(new Payment(payments.size() + 1, bill.getAmount(), bill.getId()));
                System.out.println("Payment has been completed for Bill with id " + billId + ".");
            } else {
                System.out.println("Sorry! Not enough fund to proceed with payment for bill id " + billId + ".");
            }
        }
        DecimalFormat balanceFormat = new DecimalFormat("#");
        System.out.println("Your current balance is: " + balanceFormat.format(balance));
    }

    public void schedulePayment(int billId, String scheduleDateStr) {
        Bill bill = findBillById(billId);
        if (bill == null) {
            System.out.println("Sorry! Not found a bill with id " + billId);
            return;
        }
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(Constants.dateFormat);
        LocalDate scheduleDate = LocalDate.parse(scheduleDateStr, formatter);

        if (scheduleDate.isBefore(LocalDate.now())) {
            System.out.println("Scheduled date must be in the future.");
            return;
        }

        Payment scheduledPayment = new Payment(payments.size() + 1, bill.getAmount(), bill.getId());
        scheduledPayment.setStatus(States.PENDING.name());
        scheduledPayment.setPaymentDate(Date.from(scheduleDate.atStartOfDay(ZoneId.systemDefault()).toInstant()));
        payments.add(scheduledPayment);

        System.out.println("Payment for bill id " + billId + " is scheduled on " + scheduleDateStr);
    }

    public List<Payment> listPayments() {
        return this.payments;
    }

    public List<Bill> searchBillByProvider(String provider) {
        return bills.stream()
                .filter(bill -> bill.getProvider().toLowerCase().contains(provider.toLowerCase()))
                .collect(Collectors.toList());
    }

    public static void setInstance(Customer instance) {
        Customer.instance = instance;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public List<Bill> getBills() {
        return bills;
    }

    public void setBills(List<Bill> bills) {
        this.bills = bills;
    }

    public List<Payment> getPayments() {
        return payments;
    }

    public void setPayments(List<Payment> payments) {
        this.payments = payments;
    }
}
