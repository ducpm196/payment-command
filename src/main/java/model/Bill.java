package main.java.model;

import main.java.constant.States;

import java.math.BigDecimal;
import java.util.Date;

public class Bill {
    private int id;
    private String type;
    private BigDecimal amount;
    private Date dueDate;
    private String status;
    private String provider;

    public Bill() {
    }

    public Bill(String type, BigDecimal amount, Date dueDate, String status, String provider) {
        this.type = type;
        this.amount = amount;
        this.dueDate = dueDate;
        this.status = status;
        this.provider = provider;
    }

    public Bill(int id, String type, BigDecimal amount, Date dueDate, String provider) {
        this.id = id;
        this.type = type;
        this.amount = amount;
        this.dueDate = dueDate;
        this.status = States.NOT_PAID.name();
        this.provider = provider;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }
}
