package main.java.model;

import main.java.constant.States;

import java.math.BigDecimal;
import java.util.Date;

public class Payment {
    private int id;
    private BigDecimal amount;
    private Date paymentDate;
    private String status;
    private int billId;

    public Payment() {
    }

    public Payment(BigDecimal amount, Date paymentDate, String status, int billId) {
        this.amount = amount;
        this.paymentDate = paymentDate;
        this.status = status;
        this.billId = billId;
    }

    public Payment(int id, BigDecimal amount, int billId) {
        this.id = id;
        this.amount = amount;
        this.paymentDate = new Date();
        this.status = States.PROCESSED.name();
        this.billId = billId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Date getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getBillId() {
        return billId;
    }

    public void setBillId(int billId) {
        this.billId = billId;
    }
}
