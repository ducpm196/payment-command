package main.java.constant;

public class Constants {
    public static final String dateFormat = "dd/MM/yyyy";
    public static final String billHeader = "Bill No. | Type | Amount | Due Date | State | PROVIDER";
    public static final String paymentHeader = "No. | Amount | Payment Date | State | Bill Id";
}
