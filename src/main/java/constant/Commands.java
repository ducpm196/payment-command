package main.java.constant;

public enum Commands {
    CASH_IN,
    CREATE_BILL,
    UPDATE_BILL,
    DELETE_BILL,
    LIST_BILL,
    PAY,
    DUE_DATE,
    SCHEDULE,
    LIST_PAYMENT,
    SEARCH_BILL_BY_PROVIDER,
    VIEW_BALANCE,
    EXIT
}
