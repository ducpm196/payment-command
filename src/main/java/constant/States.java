package main.java.constant;

public enum States {
    NOT_PAID,
    PENDING,
    PROCESSED
}
