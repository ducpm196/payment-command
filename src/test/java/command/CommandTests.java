package test.java.command;

import main.java.command.impl.*;
import main.java.constant.Commands;
import main.java.constant.Constants;
import main.java.model.Customer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.text.SimpleDateFormat;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class CommandTests {
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
        Customer.setInstance(null); // Reset the customer instance for a fresh start in each test
    }

    @After
    public void restoreStreams() {
        System.setOut(originalOut);
    }

    @Test
    public void testCashInCommand() {
        CashInCommand command = new CashInCommand();
        command.execute(new String[]{Commands.CASH_IN.name(), "5000"});
        assertEquals("Your available balance: 5000\r\n", outContent.toString());
    }

    @Test
    public void testCreateBillCommand() {
        CreateBillCommand command = new CreateBillCommand();
        command.execute(new String[]{Commands.CREATE_BILL.name(), "ELECTRIC", "5000", "25/07/2023", "EVN"});
        assertEquals("Bill created with ID: 4\r\n", outContent.toString());
    }

    @Test
    public void testDeleteBillCommand() {
        DeleteBillCommand command = new DeleteBillCommand();
        command.execute(new String[]{Commands.DELETE_BILL.name(), "1"});
        assertEquals("Bill with ID 1 deleted.\r\n", outContent.toString());
    }

    @Test
    public void testPayCommand() {
        CashInCommand cashInCommand = new CashInCommand();
        cashInCommand.execute(new String[]{"", "1000000"});

        PayCommand command = new PayCommand();
        command.execute(new String[]{Commands.PAY.name(), "2"});
        assertTrue(outContent.toString().contains("Payment has been completed for Bill with id 2."));
    }

    @Test
    public void testScheduleCommand() {
        ScheduleCommand command = new ScheduleCommand();
        command.execute(new String[]{Commands.SCHEDULE.name(), "3", "30/09/2023"});
        assertEquals("Payment for bill id 3 is scheduled on 30/09/2023\r\n", outContent.toString());
    }

    @Test
    public void testSearchBillByProviderCommand() {
        SearchBillByProviderCommand command = new SearchBillByProviderCommand();
        command.execute(new String[]{Commands.SEARCH_BILL_BY_PROVIDER.name(), "EVN"});
        assertTrue(outContent.toString().contains("ELECTRIC"));
    }

    @Test
    public void testUpdateBillCommand() {
        UpdateBillCommand command = new UpdateBillCommand();
        command.execute(new String[]{Commands.UPDATE_BILL.name(), "1", "WATER", "2000", "25/07/2023", "WATER_PROVIDER"});
        assertEquals("Bill with ID 1 updated.\r\n", outContent.toString());
    }

    @Test
    public void testDueDateCommand() {
        DueDateCommand command = new DueDateCommand();
        command.execute(new String[]{Commands.DUE_DATE.name()});
        SimpleDateFormat dateFormat = new SimpleDateFormat(Constants.dateFormat);
        assertTrue(outContent.toString().contains(dateFormat.format(Customer.getInstance().listBills().get(0).getDueDate())));
    }

    @Test
    public void testListBillCommand() {
        ListBillCommand command = new ListBillCommand();
        command.execute(new String[]{Commands.LIST_BILL.name()});
        assertTrue(outContent.toString().contains("ELECTRIC"));
        assertTrue(outContent.toString().contains("WATER"));
        assertTrue(outContent.toString().contains("INTERNET"));
    }

    @Test
    public void testListPaymentCommand() {
        CashInCommand cashInCommand = new CashInCommand();
        cashInCommand.execute(new String[]{"", "1000000"}); // Adding 1,000,000 to the balance

        Customer.getInstance().payBill(new Integer[]{1, 2, 3});

        ListPaymentCommand command = new ListPaymentCommand();
        command.execute(new String[]{Commands.LIST_PAYMENT.name()});

        assertTrue(outContent.toString().contains("Payment has been completed for Bill with id 1."));
        assertTrue(outContent.toString().contains("Payment has been completed for Bill with id 2."));
        assertTrue(outContent.toString().contains("Sorry! Not enough fund to proceed with payment for bill id 3."));
        assertTrue(outContent.toString().contains("200000"));
        assertTrue(outContent.toString().contains("175000"));
        // Do not check for 800000 as it was not paid
    }

    @Test
    public void testViewBalanceCommand() {
        ViewBalanceCommand command = new ViewBalanceCommand();
        command.execute(new String[]{Commands.VIEW_BALANCE.name()});
        assertTrue(outContent.toString().contains("Your current balance:"));
    }
}
