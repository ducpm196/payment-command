package test.java;

import main.java.constant.Constants;
import main.java.constant.States;
import main.java.model.Bill;
import main.java.model.Customer;
import main.java.model.Payment;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.time.LocalDate;
import java.util.List;

public class CustomerTest {

    private Customer customer;
    private SimpleDateFormat dateFormat;

    @Before
    public void setUp() {
        // Reset the customer instance for a fresh start in each test
        Customer.setInstance(null);
        customer = Customer.getInstance();
        dateFormat = new SimpleDateFormat(Constants.dateFormat);
    }

    @Test
    public void testAddFunds() {
        customer.addFunds(BigDecimal.valueOf(1000));
        assertEquals(0, BigDecimal.valueOf(1000).compareTo(customer.getBalance()));
    }

    @Test
    public void testPayBill() {
        customer.addFunds(BigDecimal.valueOf(500000));
        customer.createBill("ELECTRIC", BigDecimal.valueOf(200000), new Date(), "EVN HCMC");
        customer.payBill(new Integer[]{1});
        Bill bill = customer.findBillById(1);
        assertEquals(States.PROCESSED.name(), bill.getStatus());
    }

    @Test
    public void testCreateBill() {
        customer.createBill("WATER", BigDecimal.valueOf(100000), new Date(), "WATER HCMC");
        Bill bill = customer.findBillById(4);
        assertNotNull(bill);
    }

    @Test
    public void testDeleteBill() {
        customer.createBill("INTERNET", BigDecimal.valueOf(300000), new Date(), "ISP HCMC");
        customer.deleteBill(4);
        Bill bill = customer.findBillById(4);
        assertNull(bill);
    }

    @Test
    public void testUpdateBill() {
        customer.createBill("ELECTRIC", BigDecimal.valueOf(250000), new Date(), "EVN HCMC");
        customer.updateBill(4, "ELECTRIC", BigDecimal.valueOf(275000), new Date(), "EVN HCMC");
        Bill bill = customer.findBillById(4);
        assertEquals(0, BigDecimal.valueOf(275000).compareTo(bill.getAmount()));
    }

    @Test
    public void testSchedulePayment() {
        customer.addFunds(BigDecimal.valueOf(400000));
        customer.createBill("WATER", BigDecimal.valueOf(100000), new Date(), "WATER HCMC");
        String futureDate = LocalDate.now().plusDays(5).format(DateTimeFormatter.ofPattern(Constants.dateFormat));
        customer.schedulePayment(4, futureDate);
        Payment payment = customer.findPaymentById(1);
        assertEquals(States.PENDING.name(), payment.getStatus());
    }

    @Test
    public void testSearchBillByProvider() {
        List<Bill> bills = customer.searchBillByProvider("EVN HCMC");
        assertFalse(bills.isEmpty());
    }
}